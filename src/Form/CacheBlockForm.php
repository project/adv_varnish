<?php

namespace Drupal\adv_varnish\Form;

use Drupal\adv_varnish\RequestHandler;
use Drupal\block\BlockForm;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override form for block instance forms.
 */
class CacheBlockForm extends BlockForm {

  use StringTranslationTrait;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Sets the date formatter service.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function setDateFormatter(DateFormatterInterface $date_formatter) {
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    if ($instance instanceof self) {
      $instance->setDateFormatter($container->get('date.formatter'));
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $block = $form_state->getFormObject()->getEntity();
    $settings = $block->get('settings');

    $form['settings']['adv_varnish'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Advanced Varnish cache'),
      '#tree' => TRUE,
    ];

    // Add ESI block support.
    $form['settings']['adv_varnish']['cache']['esi'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('ESI block'),
      '#default_value' => $settings['cache']['esi'] ?? FALSE,
    ];

    // Cache time for Varnish.
    $period = [
      0,
      60,
      180,
      300,
      600,
      900,
      1800,
      2700,
      3600,
      10800,
      21600,
      32400,
      43200,
      86400,
    ];
    $period = array_map([$this->dateFormatter, 'formatInterval'], array_combine($period, $period));
    $period[0] = $this->t('No caching');

    $form['settings']['adv_varnish']['cache']['ttl'] = [
      '#type' => 'select',
      '#title' => $this->t('TTL'),
      '#default_value' => $settings['cache']['ttl'] ?? 0,
      '#options' => $period,
      '#description' => $this->t('The maximum time a page can be cached by varnish.'),
      '#states' => [
        'visible' => [
          ':input[name="settings[adv_varnish][cache][esi]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $options = [
      RequestHandler::CACHE_PER_PAGE => $this->t('Shared'),
      RequestHandler::CACHE_PER_ROLE => $this->t('Per User Roles'),
      RequestHandler::CACHE_PER_USER => $this->t('Per User ID'),
    ];

    $form['settings']['adv_varnish']['cache']['cachemode'] = [
      '#title' => $this->t('Cache granularity (Cache bin)'),
      '#description' => $this->t('Choosing those will increase cache relevance, but reduce performance.'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $settings['cache']['cachemode'] ?? RequestHandler::CACHE_PER_ROLE,
      '#states' => [
        'visible' => [
          ':input[name="settings[adv_varnish][cache][esi]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $plugin = $this->getEntity()->getPlugin();
    $config = $plugin->getConfiguration();
    $adv_varnish = $form_state->getValue('settings')['adv_varnish'];
    $config['cache'] = $adv_varnish['cache'];
    $plugin->setConfiguration($config);
  }

}
