<?php

namespace Drupal\adv_varnish\EventSubscriber;

use Drupal\adv_varnish\CacheManagerInterface;
use Drupal\adv_varnish\RequestHandler;
use Drupal\adv_varnish\RequestHandlerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\FinishResponseSubscriber;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Event subscriber class.
 */
class CacheableResponseSubscriber extends FinishResponseSubscriber {

  /**
   * A config factory for retrieving required config objects.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $advVarnishConfig;

  /**
   * Varnish request handler.
   *
   * @var \Drupal\adv_varnish\RequestHandlerInterface
   */
  protected $requestHandler;

  /**
   * The adv_varnish cookie manager.
   *
   * @var \Drupal\adv_varnish\CacheManagerInterface|\Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheManager;

  /**
   * User account interface.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * Constructs a FinishResponseSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager object for retrieving the correct language code.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\PageCache\RequestPolicyInterface $request_policy
   *   A policy rule determining the cacheability of a request.
   * @param \Drupal\Core\PageCache\ResponsePolicyInterface $response_policy
   *   A policy rule determining the cacheability of a response.
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $cache_contexts_manager
   *   The cache contexts manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\adv_varnish\RequestHandlerInterface $request_handler
   *   Varnish request handler.
   * @param \Drupal\adv_varnish\CacheManagerInterface $cache_manager
   *   Cache manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current account.
   * @param bool $http_response_debug_cacheability_headers
   *   (optional) Whether to send cacheability headers for debugging purposes.
   */
  public function __construct(LanguageManagerInterface $language_manager,
                              ConfigFactoryInterface   $config_factory,
                              RequestPolicyInterface   $request_policy,
                              ResponsePolicyInterface  $response_policy,
                              CacheContextsManager     $cache_contexts_manager,
                              TimeInterface            $time,
                              RequestHandlerInterface  $request_handler,
                              CacheManagerInterface    $cache_manager,
                              AccountProxyInterface    $account,
                              $http_response_debug_cacheability_headers = FALSE) {
    // Check if we're on Drupal 11.
    if (version_compare(\Drupal::VERSION, '11.0.0', '>=')) {
      parent::__construct(
        $language_manager,
        $config_factory,
        $request_policy,
        $response_policy,
        $cache_contexts_manager,
        $time,
        $http_response_debug_cacheability_headers
      );
    } else {
      parent::__construct(
        $language_manager,
        $config_factory,
        $request_policy,
        $response_policy,
        $cache_contexts_manager,
        $http_response_debug_cacheability_headers
      );
    }
    $this->advVarnishConfig = $config_factory->get('adv_varnish.cache_settings');
    $this->cacheManager = $cache_manager;
    $this->requestHandler = $request_handler;
    $this->account = $account;
  }

  /**
   * Response event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $event->getRequest();
    /** @var \Symfony\Component\HttpFoundation\Response $response */
    $response = $event->getResponse();

    // We need to clear User blocks on POST submissions,
    // as it can update relevant user info.
    $this->cacheManager->purgeUserBlocks();

    // Set the Content-language header.
    $response->headers->set('Content-language', $this->languageManager->getCurrentLanguage()->getId());

    $is_cacheable = $this->cacheManager->cachingEnabled();

    // Add headers necessary to specify whether the response should be cached by
    // proxies and/or the browser.
    if ($is_cacheable && $response instanceof CacheableResponseInterface && $this->advVarnishConfig->get('available.enable_cache')) {
      // Set Cacheable response.
      $this->setResponseCacheable($response, $request);
      $this->requestHandler->handleResponseEvent($event);
    }
    else {
      // If either the policy forbids caching or the sites configuration does
      // not allow to add a max-age directive, then enforce a Cache-Control
      // header declaring the response as not cacheable.
      $this->setResponseNotCacheable($response, $request);
      $response->headers->set('X-Pass-Varnish', 'YES', FALSE);
      $response->headers->set(RequestHandler::HEADER_ADV_VARNISH_STATUS, 'Cache-disabled', FALSE);
    }
  }

}
